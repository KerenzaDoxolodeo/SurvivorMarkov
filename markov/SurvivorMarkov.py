import os
import markovify

markovDictionary = {}

print(os.listdir('.'))
files = [f for f in os.listdir('markov_model')]
for I in files:
    name = I.split(".json")
    name = name[0].split("!")
    season = eval(name[1])
    name = name[0]

    with open('markov_model/'+I,"r",encoding='utf8') as f:
        model_json = f.read()
        model_json = markovify.Text.from_json(model_json)
        markovDictionary[name] = [model_json,season]
        print(name,season)

def getList():
    nameList = [I for I in markovDictionary]
    nameList.sort()
    return [{'name':I,'season':"(%s)"% ",".join(markovDictionary[I][1])} for I in nameList]

def getMarkovModel(name):
    if name in markovDictionary:
        return markovDictionary[name][0]
    return None

