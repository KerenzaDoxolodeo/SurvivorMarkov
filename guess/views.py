from django.shortcuts import render
from markov import SurvivorMarkov
from random import sample,randint
response = {}

def index(request):
    response['title'] = "Survivor - Who said dat?"
    html = 'guess/guess.html'
    choice = sample(SurvivorMarkov.getList(),3)
    m = randint(0,2)
    answer = ['A','B','C'][m]
    response['optionA'] = choice[0]['name']
    response['optionB'] = choice[1]['name']
    response['optionC'] = choice[2]['name']
    response['answer'] = answer
    model = SurvivorMarkov.getMarkovModel(choice[m]['name'])
    if model != None:
        result = ""
        while True:
            generated = model.make_sentence(test_output = False)
            if len(result + " " + generated) > 500:
                break
            result += " "+ generated
        response['text'] = result
    return render(request, html, response)