from django.shortcuts import render
from markov import SurvivorMarkov
import markovify
from django.views.decorators.csrf import csrf_exempt
response = {}

@csrf_exempt
def index(request):
    name1 = None
    name2 = None
    model1 = None
    model2 = None
    error = False
    response['choice'] = SurvivorMarkov.getList()
    if request.method == "POST":
        try:
            name1 = request.POST['dropdown1']
            model1 = SurvivorMarkov.getMarkovModel(name1)
            name2 = request.POST['dropdown2']
            model2 = SurvivorMarkov.getMarkovModel(name2)
            if name1 == name2:
                error = True
        except:
            name1 = None
            name2 = None
            model1 = None
            model2 = None
    if error == True:
        response['error'] = True
        html = 'wiggle/wiggle-blank.html'
        response['title'] = "SurvivorMarkov - Wiggle Room"
        return render(request, html, response)
    if model1 != None and model2 != None:
        combinedModel = markovify.combine([model1,model2])
        result = ""
        while True:
            generated = combinedModel.make_sentence(test_output = False)
            if len(result + " " + generated) > 1000:
                break
            result += " "+ generated
        response['text'] = result
    if name1 == None or name2 == None:
        html = 'wiggle/wiggle-blank.html'
        response['title'] = "SurvivorMarkov - Wiggle Room"
        return render(request, html, response)
    html = 'wiggle/wiggle.html'
    response['title'] = "SurvivorMarkov - %s & %s" % (name1,name2)
    response['chosen1'] = name1
    response['chosen2'] = name2
    return render(request, html, response)