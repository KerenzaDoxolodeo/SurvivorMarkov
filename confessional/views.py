from django.shortcuts import render
from markov import SurvivorMarkov
from django.views.decorators.csrf import csrf_exempt
response = {}

def index(request):
    html = 'confessional/index.html'
    response['title'] = 'SurvivorMarkov'
    response['choice'] = SurvivorMarkov.getList()
    return render(request, html, response)

@csrf_exempt
def full(request):
    name = None
    model = None
    response['choice'] = SurvivorMarkov.getList()
    if request.method == "POST":
        try:
            name = request.POST['dropdown']
            model = SurvivorMarkov.getMarkovModel(name)
        except:
            name = None
            model = None
    if model != None:
        result = ""
        while True:
            generated = model.make_sentence(test_output = False)
            if len(result + " " + generated) > 500:
                break
            result += " "+ generated
        response['text'] = result
    if name == None:
        html = 'confessional/full-blank.html'
        response['title'] = "SurvivorMarkov"
        return render(request, html, response)
    html = 'confessional/full.html'
    response['title'] = "SurvivorMarkov - %s" % (name)
    response['chosen'] = name
    return render(request, html, response)

@csrf_exempt
def tweet(request):
    name = None
    model = None
    response['choice'] = SurvivorMarkov.getList()
    if request.method == "POST":
        try:
            name = request.POST['dropdown']
            model = SurvivorMarkov.getMarkovModel(name)
        except:
            name = None
            model = None
    if model != None:
        result = ""
        while True:
            generated = model.make_sentence(test_output = False)
            if len(result + " " + generated) > 100:
                if len(result) > 0:
                    break
            else:
                result += " "+ generated
        response['text'] = result
    if name == None:
        html = 'confessional/tweet-blank.html'
        response['title'] = "SurvivorMarkov"
        return render(request, html, response)
    html = 'confessional/tweet.html'
    response['title'] = "SurvivorMarkov - %s" % (name)
    response['chosen'] = name
    return render(request, html, response)