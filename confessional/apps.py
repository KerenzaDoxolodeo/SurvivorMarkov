from django.apps import AppConfig


class ConfessionalConfig(AppConfig):
    name = 'confessional'
