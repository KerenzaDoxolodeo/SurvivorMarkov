from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^full/$', full, name='full'),
    url(r'^tweetable/$',tweet, name='tweetable')
]
